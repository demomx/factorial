# Factorial API

It is an API used for calculating factorials of two numbers `a` and `b` 
without any restrictions, as this API uses `big.Int` type for numbers. 
The best to calculate are the numbers with restriction 0 ≤ n ≤ 10^5. 
You can use bigger numbers, but the process takes more than 1 minute. 
As a future opportunity, the queue for numbers more than 10^5 is possible.


## Usage
For calculating factorial you should send `POST` request 
to url `http://localhost:8989/calculate` with data.

### Example
Your request:
```json
{
  "a": 10,
  "b": 2
}
```

Your response:
```json
{
  "a!": 3628800,
  "b!": 2
}
```
