FROM golang:latest
LABEL maintainer="Vladimir Goncharov <demomx1998@gmail.com>"

WORKDIR /app
COPY ./app/go.mod ./app/go.sum ./
RUN go mod download
COPY ./app .
RUN go build -o app .

EXPOSE 8989

CMD ["./app"]