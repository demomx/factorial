package main

import (
	"encoding/json"
	"errors"
	"io"
	"math/big"
)

type CalculateRequest struct {
	A big.Int `json:"a"`
	B big.Int `json:"b"`
}

type CalculateErrorResponse struct {
	ExistError bool   `json:"-"`
	Message    string `json:"error"`
}

func ReadModelFrom(model interface{}, reader io.Reader) error {
	if reader == nil {
		return errors.New("undefined reader")
	}
	jsonReader := json.NewDecoder(reader)
	return jsonReader.Decode(model)
}

func CompileModelTo(model interface{}, writer io.Writer) error {
	if writer == nil {
		return errors.New("undefined writer")
	}
	jsonWriter := json.NewEncoder(writer)
	return jsonWriter.Encode(model)
}
