package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestCalculateValidatorMiddleware(t *testing.T) {
	doInner := false
	handler := CalculateValidatorMiddleware(func(writer http.ResponseWriter, request *http.Request) {
		doInner = true
	})
	response := httptest.NewRecorder()
	requestString := "{\"a\":10,\"b\":0}"
	body := ioutil.NopCloser(bytes.NewReader([]byte(requestString)))
	request := httptest.NewRequest("POST", "http://example.com:8989/calculate", body)
	handler(response, request)
	resultResponse := response.Result()
	if _, errReadResultBody := ioutil.ReadAll(resultResponse.Body); errReadResultBody != nil {
		t.Fatalf(errReadResultBody.Error())
	} else {
		if resultResponse.StatusCode != http.StatusOK {
			t.Fatalf("Incorrect status response shoud be %d, but get %d for request %q",
				http.StatusOK, resultResponse.StatusCode, requestString)
		}
	}
	if !doInner {
		t.Fatal("Code should be started into middleware")
	}
}

func TestCalculateValidatorMiddlewareNegative(t *testing.T) {
	handler := CalculateValidatorMiddleware(func(writer http.ResponseWriter, request *http.Request) {
		t.Fatal("It part can't do")
	})
	for _, requestString := range []string{"{\"a\":\"10\",\"b\":\"12\"}", "{\"a\":10,\"b\":-1}"} {
		response := httptest.NewRecorder()
		body := ioutil.NopCloser(bytes.NewReader([]byte(requestString)))
		request := httptest.NewRequest("POST", "http://example.com:8989/calculate", body)
		handler(response, request)
		resultResponse := response.Result()
		if bodyResult, errReadResultBody := ioutil.ReadAll(resultResponse.Body); errReadResultBody != nil {
			t.Fatalf(errReadResultBody.Error())
		} else {
			if resultResponse.StatusCode != http.StatusBadRequest {
				t.Fatalf("Incorrect status response shoud be %d, but get %d for request %q",
					http.StatusBadRequest, resultResponse.StatusCode, requestString)
			}
			if strings.Compare(strings.TrimSpace(string(bodyResult)), "{\"error\":\"Incorrect data\"}") != 0 {
				t.Fatalf("Get incorrect answer %q", string(bodyResult))
			}
		}
	}
}

func TestCalculateEndpoint(t *testing.T) {
	response := httptest.NewRecorder()
	requestString := "{\"a\":3,\"b\":0}"
	body := ioutil.NopCloser(bytes.NewReader([]byte(requestString)))
	request := httptest.NewRequest("POST", "http://example.com:8989/calculate", body)
	CalculateEndpoint(response, request)
	resultResponse := response.Result()
	if content, errReadResultBody := ioutil.ReadAll(resultResponse.Body); errReadResultBody != nil {
		t.Fatalf(errReadResultBody.Error())
	} else {
		if resultResponse.StatusCode != http.StatusOK {
			t.Fatalf("Incorrect status response shoud be %d, but get %d for request %q",
				http.StatusOK, resultResponse.StatusCode, requestString)
		}
		jsonReader := json.NewDecoder(bytes.NewReader(content))
		result := make(map[string]int)
		if errDecode := jsonReader.Decode(&result); errDecode != nil {
			t.Fatal(errDecode.Error())
		} else {
			if result["a!"] != 6 || result["b!"] != 1 {
				t.Fatal("Incorrect answer")
			}
		}
	}
}

func TestCalculateEndpointNegative(t *testing.T) {
	for _, requestString := range []string{"{\"a\":\"10\",\"b\":\"12\"}", "{\"a\":10,\"b\":-1}", ""} {
		response := httptest.NewRecorder()
		body := ioutil.NopCloser(bytes.NewReader([]byte(requestString)))
		request := httptest.NewRequest("POST", "http://example.com:8989/calculate", body)
		CalculateEndpoint(response, request)
		resultResponse := response.Result()
		if _, errReadResultBody := ioutil.ReadAll(resultResponse.Body); errReadResultBody != nil {
			t.Fatalf(errReadResultBody.Error())
		} else {
			if resultResponse.StatusCode != http.StatusInternalServerError {
				t.Fatalf("Incorrect status response shoud be %d, but get %d for request %q",
					http.StatusInternalServerError, resultResponse.StatusCode, requestString)
			}
		}
	}
}
