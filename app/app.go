package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"math/big" // I am use big.Int for calculate the biggest numbers
	"net/http"
	"os"

	"github.com/julienschmidt/httprouter"
)

var (
	Router = httprouter.New()
	Logger = log.New(os.Stdout, "[calculator] ", log.Ldate|log.Ltime|log.Llongfile)
)

func init() {
	Router.HandlerFunc("POST", "/calculate", CalculateValidatorMiddleware(CalculateEndpoint))
}

func CalculateEndpoint(response http.ResponseWriter, request *http.Request) {
	requestData := new(CalculateRequest)
	if err := ReadModelFrom(&requestData, request.Body); err == nil {
		factorial := NewFactorial()
		if result, errCalculate := factorial.Calculate(&requestData.A, &requestData.B); errCalculate != nil {
			response.WriteHeader(http.StatusInternalServerError)
			if errW := CompileModelTo(CalculateErrorResponse{
				Message: errCalculate.Error(),
			}, response); errW != nil {
				response.WriteHeader(http.StatusInternalServerError)
				Logger.Println(errW, errCalculate)
			}
		} else {
			if errW := CompileModelTo(map[string]*big.Int{
				"a!": result[requestData.A.String()],
				"b!": result[requestData.B.String()],
			}, response); errW != nil {
				response.WriteHeader(http.StatusInternalServerError)
				Logger.Println(errW)
			}
		}
	} else {
		response.WriteHeader(http.StatusInternalServerError)
		Logger.Println(err)
	}
}

func CalculateValidatorMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return func(response http.ResponseWriter, request *http.Request) {

		response.Header().Set("Content-Type", "application/json; charset=UTF-8")

		requestData := new(CalculateRequest)
		errResponse := new(CalculateErrorResponse)
		errResponse.Message = "Incorrect data"
		if buf, errReadBody := ioutil.ReadAll(request.Body); errReadBody != nil {
			errResponse.ExistError = true
		} else {
			request.Body = ioutil.NopCloser(bytes.NewBuffer(buf))
			if err := ReadModelFrom(&requestData, request.Body); err != nil {
				errResponse.ExistError = true
				Logger.Println(err)
			} else {
				errResponse.ExistError =
					requestData.B.Cmp(big.NewInt(0)) < 0 ||
					requestData.A.Cmp(big.NewInt(0)) < 0
			}
			request.Body = ioutil.NopCloser(bytes.NewBuffer(buf))
		}
		if errResponse.ExistError {
			response.WriteHeader(http.StatusBadRequest)
			if errCompile := CompileModelTo(errResponse, response); errCompile != nil {
				response.WriteHeader(http.StatusInternalServerError)
				Logger.Println(errCompile)
			}
		} else {
			next.ServeHTTP(response, request)
		}
	}
}

func main() {
	Logger.Fatal(http.ListenAndServe("0.0.0.0:8989", Router))
}
