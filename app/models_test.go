package main

import (
	"bytes"
	"io/ioutil"
	"math/big"
	"strings"
	"testing"
)

var testBodyRequest = "{\"a\":10,\"b\":12}"

type testResponse struct {
	A *big.Int `json:"a"`
	B *big.Int `json:"b"`
}

func TestReadModelFrom(t *testing.T) {
	reader := ioutil.NopCloser(bytes.NewReader([]byte(testBodyRequest)))
	model := new(testResponse)
	if errNilReader :=  ReadModelFrom(&model, nil); errNilReader == nil {
		t.Fatal("Must be error when reader is nil")
	}
	if errParse := ReadModelFrom(&model, reader); errParse != nil {
		t.Fatal(errParse.Error())
	}
	if model.A.Cmp(big.NewInt(10)) != 0 || model.B.Cmp(big.NewInt(12)) != 0 {
		t.Fatal("Incorrect parsing ")
	}
}

func TestCompileModelTo(t *testing.T) {
	model := new(testResponse)
	model.A = big.NewInt(10)
	model.B = big.NewInt(12)
	writer := bytes.NewBufferString("")
	if errNilWriter :=  CompileModelTo(&model, nil); errNilWriter == nil {
		t.Fatal("Must be error when writer is nil")
	}
	if errWrite := CompileModelTo(model, writer); errWrite != nil {
		t.Fatal(errWrite.Error())
	}
	if strings.Compare(strings.TrimSpace(writer.String()), strings.TrimSpace(testBodyRequest)) != 0{
		t.Fatalf("incorrect write")
	}
}
