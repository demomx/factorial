package main

import (
	"errors"
	"math/big"
	"sync"
)

type partCalculate struct {
	start *big.Int
	stop  *big.Int
}

type FactorialResult struct {
	Input  *big.Int
	Output *big.Int
}

type Factorial struct {
	limitSubWorkers int
}

func NewFactorial() (factorial *Factorial) {
	factorial = &Factorial{}
	factorial.limitSubWorkers = 20
	return
}

func (f *Factorial) GetLimitOperation() int{
	return f.limitSubWorkers
}

func (f *Factorial) SetLimitOperation(limit int) int{
	if limit > 20 {
		f.limitSubWorkers = limit
	}
	return f.limitSubWorkers
}


func (f *Factorial) Calculate(numbers ...*big.Int) (result map[string]*big.Int, err error) {
	if err = f.validateNumbers(numbers...); err != nil {
		return
	}
	inputChanel := make(chan *FactorialResult, len(numbers))
	outputChanel := make(chan *FactorialResult, len(numbers))
	wg := &sync.WaitGroup{}
	wg.Add(len(numbers))
	for _, number := range numbers {
		inputChanel <- &FactorialResult{Input: number, Output: big.NewInt(1)}
		go f.Worker(inputChanel, outputChanel, wg)
	}
	close(inputChanel)
	wg.Wait()
	close(outputChanel)

	result = map[string]*big.Int{}

	for res := range outputChanel {
		result[res.Input.String()] = res.Output
	}
	return
}

func (f *Factorial) validateNumbers(numbers ...*big.Int) (err error) {
	if len(numbers) == 0 {
		err = errors.New("get empty params")
		return
	}
	for _, number := range numbers {
		if number.Cmp(big.NewInt(0)) == -1 {
			err = errors.New("get negative number")
			return
		}
	}
	return
}

func (f *Factorial) Worker(inputChanel chan *FactorialResult, outputChanel chan *FactorialResult, wg *sync.WaitGroup) {
	defer wg.Done()
	select {
	case result := <-inputChanel:
		counter := new(big.Int).Set(result.Input)
		if counter.Cmp(big.NewInt(int64(f.limitSubWorkers))) == 1 {
			subInputChan := make(chan *partCalculate, f.limitSubWorkers)
			subOutputChan := make(chan *big.Int, f.limitSubWorkers)
			subWg := &sync.WaitGroup{}
			subWg.Add(f.limitSubWorkers)

			step, mod := new(big.Int).DivMod(counter, big.NewInt(int64(f.limitSubWorkers)), big.NewInt(1))
			startPosition := new(big.Int).Sub(counter, mod)

			for i := f.limitSubWorkers; i > 0; i -= 1 {
				if startPosition.Cmp(big.NewInt(0)) == -1 {
					subWg.Done()
				} else {
					part := &partCalculate{
						start: new(big.Int).Set(startPosition),
						stop:  new(big.Int).Sub(startPosition, step),
					}

					if part.stop.Cmp(big.NewInt(1)) == -1 {
						part.stop.Set(big.NewInt(1))
					}
					subInputChan <- part
					startPosition.Set(part.stop).Sub(startPosition, big.NewInt(1))
					go f.subWorker(subInputChan, subOutputChan, subWg)
				}
			}
			close(subInputChan)
			subWg.Wait()
			close(subOutputChan)
			for mod.Cmp(new(big.Int)) == 1 {
				result.Output.Mul(result.Output, counter)
				mod.Sub(mod, big.NewInt(1))
				counter.Sub(counter, big.NewInt(1))
			}
			for resultNumber := range subOutputChan {
				result.Output.Mul(result.Output, resultNumber)
			}
		} else {
			backCounter := big.NewInt(1)
			result.Output = f.calculate(counter, backCounter)
		}
		outputChanel <- result
	}
}

func (f *Factorial) subWorker(inputChanel chan *partCalculate, outputChan chan *big.Int, wg *sync.WaitGroup) {
	defer wg.Done()
	select {
	case data := <-inputChanel:
		outputChan <- f.calculate(data.start, data.stop)
	}
}

func (f *Factorial) calculate(start *big.Int, stop *big.Int) (result *big.Int) {
	result = big.NewInt(1)
	incrementDecrement := big.NewInt(1)
	for start.Cmp(stop) == 1 {
		result.Mul(result, start)
		result.Mul(result, stop)
		start.Sub(start, incrementDecrement)
		stop.Add(stop, incrementDecrement)
	}
	if start.Cmp(stop) == 0 {
		result.Mul(result, start)
	}
	return
}
