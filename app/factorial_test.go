package main

import (
	"math/big"
	"testing"
)

type FactorialInterface interface {
	Calculate(...big.Int) (map[string]string, error)
}

var factorial *Factorial = NewFactorial()
var testNumbers = map[*big.Int]*big.Int{
	big.NewInt(5):  big.NewInt(120),
	big.NewInt(12): big.NewInt(479001600),
	big.NewInt(0):  big.NewInt(1),
}

func TestFactorial_Calculate(t *testing.T) {
	if _, errEmpty := factorial.Calculate(); errEmpty == nil {
		t.Fatal("Method Calculate should return err about empty numbers")
	}

	var cases = make([]*big.Int, 0)

	for k := range testNumbers {
		cases = append(cases, k)
	}

	if result, errStandardCases := factorial.Calculate(cases...); errStandardCases == nil {
		for k, v := range testNumbers {
			if result[k.String()].Cmp(v) != 0 {
				t.Fatalf("For number %q should be %q, but it is %q",
					k.String(), v.String(), result[k.String()].String())
			}
		}
	}

	if _, errMinusValue := factorial.Calculate(big.NewInt(-12)); errMinusValue == nil {
		t.Fatal("Method Calculate should return err about negative numbers")
	}

	// test for minimal sets
	if factorial.SetLimitOperation(10) != 20 {
		t.Fatal("Limit for factorials should be 20")
	}
	if newLimit := factorial.SetLimitOperation(40); newLimit != 40 {
		t.Fatalf("Limit for factorials should be 40, but that equals %d", newLimit)
	}
	if oldLimit := factorial.GetLimitOperation(); oldLimit != 40 {
		t.Fatalf("Limit for factorials should be 40, but that equals %d", oldLimit)
	}

	testNumber := big.NewInt(50)
	biggestResult := new(big.Int)
	biggestResult.SetString("30414093201713378043612608166064768844377641568960512000000000000", 0)
	if result, errBiggest := factorial.Calculate(testNumber); errBiggest != nil {
		t.Fatal(errBiggest)
	} else {
		if result[testNumber.String()].Cmp(biggestResult) != 0 {
			t.Fatalf("Method should return %q, but get %q", biggestResult.String(), result[testNumber.String()])
		}
	}

}
